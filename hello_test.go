// version 2.0


package guestbook

import (
//	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"net/url"
	"appengine/aetest"
	"appengine/datastore"
	"strings"
)

/*
func TestRoot(t *testing.T) {

	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatalf("Could not get a context - %v", err)
	}
	defer c.Close()

    url, _ := url.Parse("http://localhost:8080/?nameOfBook=test")
    var r http.Request
    r.URL = url
    var w http.ResponseWriter
    root(c, w, &r)

//	fmt.Println("aaaa")

//	s := w.WriteHeader(0)
//	fmt.Println(s)


}*/

func TestRoot(t *testing.T) {
	c, err := aetest.NewContext(nil)
    if err != nil {
        t.Fatalf("Could not get a context - %v", err)
    }
    defer c.Close()

	request, _ := http.NewRequest("GET", "http://localhost:8080/?nameOfBook=test", nil)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	response := httptest.NewRecorder()

	root(c, response, request)

	//check if the StatusCode is 200 or not
    if response.Code != http.StatusOK {
        t.Fatalf("Non-expected status code%v:\n\tbody: %v", "200", response.Code)
    }

	//check if nameOfBook is test or not
	s := response.Body.String()
	if strings.Contains(s,  "/sign/test") != true {
		t.Fatalf("nameOfBook is not set")
	}
	if strings.Contains(s, "value=\"test\"") != true {
		t.Fatalf("nameOfBook is not set")
	}

}

func TestSign(t *testing.T) {

    c, err := aetest.NewContext(nil)
    if err != nil {
        t.Fatalf("Could not get a context - %v", err)
    }
    defer c.Close()

	data := url.Values{}
	data.Add("content", "testString")
	b := strings.NewReader(data.Encode())
	request, _ := http.NewRequest("POST", "http://localhost:8080/sign/test", b)
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	response := httptest.NewRecorder()

	sign(c, response, request)

	//check if the StatusCode is redirect or not
	if response.Code != http.StatusFound {
		t.Fatalf("Non-expected status code%v:\n\tbody: %v", "302", response.Code)
	}
	q := datastore.NewQuery("Greeting").Ancestor(guestbookKey(c, "test")).Order("-Date").Limit(10)
	greetings := &[]Greeting{}
	if _, err := q.GetAll(c, greetings); err != nil {
        http.Error(response, err.Error(), http.StatusInternalServerError)
        return
    }


	//check if the datastore is increased or not
	a := len(*greetings)
	if a != 1 {
		t.Fatalf("the datastore is not increased.")
	}

	//check if the stored message is same or not
	if ((*greetings)[0].Content) != "testString" {
		t.Fatalf("the stored message is not same")
	}

}

