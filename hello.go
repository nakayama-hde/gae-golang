// version 2.1

package guestbook

import (
	"net/http"
	"html/template"
	"time"
	"fmt"
//	"os"
	"appengine"
	"appengine/datastore"
	"appengine/user"
)

type Greeting struct {
	Author string
	Content string
	Date time.Time
}

type Info struct {
	CurrentUser *user.User
	Greetings *[]Greeting
	LoginStr string
	LoginUrl string
	Guestbook_name string
}

type ContextHandler struct {
	Real func(appengine.Context, http.ResponseWriter, *http.Request)
}

const DEFAULT_GUESTBOOK_NAME =  "default_guestbook"

func init() {
	http.Handle("/", ContextHandler{root})
	http.Handle("/sign/", ContextHandler{sign})
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
}

func (f ContextHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	f.Real(c, w, r)
}


// guestbookKey returns the key used for all guestbook entries.
func guestbookKey(c appengine.Context, guestbook_name string) *datastore.Key {
	//The string "default_guetbook" here could be varied to have multiple guestbooks.
	return datastore.NewKey(c, "Guestbook", guestbook_name, 0, nil)
}


func root(c appengine.Context, w http.ResponseWriter, r *http.Request) {
	//c := appengine.NewContext(r)

	var argGuestbook_name = r.FormValue("nameOfBook")
	if argGuestbook_name == "" {
		argGuestbook_name = DEFAULT_GUESTBOOK_NAME
	}

	var argLoginUrl string
	var argLoginStr string
	u := user.Current(c)

	if u == nil {
		argLoginStr = "Login"
		url, err := user.LoginURL(c, "/?nameOfBook=" + argGuestbook_name)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		argLoginUrl = url

	} else {
		argLoginStr = "Logout"
		url, err := user.LogoutURL(c, "/?nameOfBook=" + argGuestbook_name)
        if err != nil {
            http.Error(w, err.Error(), http.StatusInternalServerError)
            return
        }
		argLoginUrl = url
	}


	q := datastore.NewQuery("Greeting").Ancestor(guestbookKey(c, argGuestbook_name)).Order("-Date").Limit(10)
	//greetings := make([]Greeting, 0, 10)
	greetings := &[]Greeting{}



	if _, err := q.GetAll(c, greetings); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	argInfo := Info{
		LoginStr : argLoginStr,
		LoginUrl : argLoginUrl,
		CurrentUser : u,
		Greetings : greetings,
		Guestbook_name : argGuestbook_name,
	}


	if err := guestbookTemplate.Execute(w, argInfo); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

var guestbookTemplate = template.Must(template.New("base").ParseFiles("template/base.html"))

func sign(c appengine.Context, w http.ResponseWriter, r *http.Request) {
//	c := appengine.NewContext(r)

	argGuestbook_name := r.URL.Path[6:]
	if argGuestbook_name == "" {
		argGuestbook_name = DEFAULT_GUESTBOOK_NAME
	}


	g := Greeting {
		Content: r.FormValue("content"),
		Date: time.Now(),
	}
	fmt.Println("g : " + g.Content)

	if u := user.Current(c); u != nil {
		g.Author = u.String()
	}

	// We set the same parent key on every Greeting entity to ensure each Greeting
	// is in the same entity group. Queries across the single entity group
	// will be consistent. However, the write rate to a single entity group
	// should be limited to ~1/second.
	key := datastore.NewIncompleteKey(c, "Greeting", guestbookKey(c, argGuestbook_name))
	_, err := datastore.Put(c, key, &g)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/?nameOfBook=" + argGuestbook_name, http.StatusFound)
}
